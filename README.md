![vhdl_gen](https://gitlab.com/Sturla22/vhdl_gen/-/raw/master/docs/img/logo.png)


# VHDL Generator (vhdl_gen)

`vhdl_gen` is a data-driven code generator for VHDL. Supply `vhdl_gen` with a yaml file,
and it will create everything from single VHDL files to VUnit `run.py` scripts to complete projects.


## Badges

[![GPLv3 License](https://img.shields.io/pypi/l/vhdl_gen?style=for-the-badge)](https://gitlab.com/sturla22/vhdl_gen/LICENSE)
[![Code coverage](https://img.shields.io/gitlab/coverage/Sturla22/vhdl_gen/master?style=for-the-badge)](https://gitlab.com/sturla22/vhdl_gen)
[![Pipeline](https://img.shields.io/gitlab/pipeline/Sturla22/vhdl_gen/master?style=for-the-badge)](https://gitlab.com/sturla22/vhdl_gen)
[![PyPi downloads](https://img.shields.io/pypi/dm/vhdl_gen?style=for-the-badge)](https://pypi.org/project/vhdl_gen/)
[![PyPi version](https://img.shields.io/pypi/v/vhdl_gen?style=for-the-badge)](https://pypi.org/project/vhdl_gen/)
[![PyPi python version](https://img.shields.io/pypi/pyversions/vhdl_gen?style=for-the-badge)](https://pypi.org/project/vhdl_gen/)



## Installation

(soon) Install `vhdl_gen` with pip

```bash
  pip install vhdl_gen
```

### Local
Until this gets uploaded to PyPi:

Clone the project

```bash
  git clone https://gitlab.com/sturla22/vhdl_gen
```

Go to the project directory

```bash
  cd vhdl_gen
```

Install dependencies

```bash
  pipenv install
```

Run the script

```bash
  python -m vhdl_gen
```

## Contributing

Contributions are always welcome!

See the contribution guidelines (CONTRIBUTING.md) for ways to get started.

## Documentation

You can find the [documentation here](https://sturla22.gitlab.io/vhdl_gen)

## Features

- Templates included for common VHDL constructs
- Configure template instantiation with yaml
- Customize templates
- Templates for projects/multiple files supported

## Roadmap

- Improve docs
- Full VHDL feature support through yaml configurations
- Add tests
    - test with behave
    - Check generated vhdl with vsg
- include vsg rules in yaml?
- improve logging
    - warn on undefined in templates: include template name
    - nicer formatting
    - debug logging
- New templates:
    - Two process architecture
    - UVVM VVC generator
    - OSVVM VC generator

## Tech Stack

**Template engine:** [Jinja2](https://jinja2docs.readthedocs.io/en/stable/)

**Data parser:** [Pyyaml](https://pyyaml.org/)

## Used By

Submit a merge request to be added here!

## Usage/Examples

With a yaml file like the following:

```yaml
!VhdlFile
author: Sturla Lange
header: !header
  extras:
    ref: myref
  detail: |
    long description
  brief: short description
libraries:
  - name: work
    use:
      - some_pkg.this
      - some_pkg.this
      - some_pkg.that
      - some_pkg.hat
  - name: ieee
    use:
      - std_logic_1164.all
entity: !entity
  name: example_e
  ports:
    - [clk, in, std_ulogic]
    - [rst, in, std_ulogic]
  generics:
    - [G_DEBUG, integer, "0"]
    - [G_SKIPTESTS, integer, '""']
architecture: !architecture
  name: structural
  constants:
    - [C_HELLO, string, '"hi"']
    - [C_WORLD, string, '"world"']
```
The following command will generate a VHDL file:
```bash
vhdl_gen --yaml examples/vhdl_file_example.yaml --template entity_architecture.vhd
```

The result is
```vhdl
--! \author Sturla Lange
--! \date 2021-05-02 21:50
--! \ref myref
--! \brief short description
--! \detail
--!         long description
library work;
  use work.some_pkg.this;
  use work.some_pkg.this;
  use work.some_pkg.that;
  use work.some_pkg.hat;
library ieee;
  use ieee.std_logic_1164.all;
entity example_e is
  generic(
      G_DEBUG: integer := 0;
      G_SKIPTESTS: integer := ""
    );
  port(
      clk: in std_ulogic;
      rst: in std_ulogic
    );
begin
end entity;
architecture structural of example_e is
  constant C_HELLO: string := "hi";
  constant C_WORLD: string := "world";



begin

end architecture;
```
## Feedback/Support

For support or feedback, submit an issue at [gitlab.com/sturla22/vhdl_gen](gitlab.com/sturla22/vhdl_gen)

## Authors

- [Sturla Lange](https://sturla22.github.io/about)


## Acknowledgements

This package was created with Cookiecutter and the [sourcery-ai/python-best-practices-cookiecutter](https://github.com/sourcery-ai/python-best-practices-cookiecutter) project template.

The model-view-controller architecture was heavily inspired by [django](https://djangoproject.com).
