Clone the project

```bash
  git clone https://gitlab.com/sturla22/vhdl_gen
```

Go to the project directory

```bash
  cd vhdl_gen
```

Install dependencies

```bash
  pipenv install --dev
```

To run tests:

```bash
  pytest
```

Run the script

```bash
  python -m vhdl_gen
```

Install the pre-commit and pre-push hooks:

```bash
pipenv run pre-commit install -t pre-commit
pipenv run pre-commit install -t pre-push
```

Scripts for automation can be found in the `Pipfile`
