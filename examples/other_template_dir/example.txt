Date: {{ env.date | datetimeformat("%Y-%m-%d %H:%M") }}
File: {{ env.template_path }}
Author: {{ vars.author }}
Description:
    {{ vars.detail | wordwrap | indent }}
