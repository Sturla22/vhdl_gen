import abc
import dataclasses
import re
from typing import Any


@dataclasses.dataclass
class LanguageViewChoice(abc.ABC):
    view: Any = None
    pattern: str = r""
    name: str = ""

    def get_name(self):
        return self.name.lower()


@dataclasses.dataclass
class LanguageViewCollection(abc.ABC):
    default_view: Any
    view_choices: list[LanguageViewChoice] = dataclasses.field(default_factory=list)

    def get_from_pattern(self, pattern):
        filtered_view_choices = filter(
            lambda view_choice: re.match(view_choice.pattern, pattern),
            self.view_choices,
        )

        try:
            return next(filtered_view_choices).view
        except StopIteration:
            # No views match the pattern.
            pass

    def get_from_name(self, name):
        if self.is_supported_name(name):
            filtered_view_choices = filter(
                lambda view_choice: view_choice.get_name() == name.lower(),
                self.view_choices,
            )
            return next(filtered_view_choices).view
        else:
            raise UserWarning(
                f"Language {name} is not supported, results may not be as expected."
            )

    def is_supported_name(self, name):
        return name.lower() in [
            view_choice.get_name() for view_choice in self.view_choices
        ]

    def get_view(self, *args, template="", language="", **kwargs):
        view = self.get_from_pattern(str(template))
        if language:
            view = self.get_from_name(language)
        if view is None:
            view = self.default_view
        return view
