import pathlib
from typing import Any

from .vhdl_models import VhdlFile
from .vhdl_models import VhdlProject
from .views import ModelView


class VhdlFileView(ModelView):
    """Generate a VHDL file from yaml."""

    model: Any = VhdlFile
    line_statement_prefix = "-->"
    line_comment_prefix = "--##"
    block_start_string = "--%"
    block_end_string = "%--"
    comment_start_string = "--#"
    comment_end_string = "#--"
    template_filename = "entity_architecture.vhd"
    template_directory = pathlib.Path("templates")
    output_directory = pathlib.Path("build")

    def add_globals(self, *args, **kwargs):
        self.env_globals["vhdl"] = True
        super().add_globals(*args, **kwargs)


class VhdlProjectView(VhdlFileView):
    """Generate a VHDL project from yaml."""

    model: Any = VhdlProject
    output_directory = pathlib.Path("build/project")
