from .models import YamlModel
from .yaml_constructors import ArchitectureYamlObject
from .yaml_constructors import EntityYamlObject
from .yaml_constructors import VhdlFileYamlObject


class VhdlFile(YamlModel):
    """Model of an VHDL file."""

    vhdl_constructors = [
        ArchitectureYamlObject,
        EntityYamlObject,
        VhdlFileYamlObject,
    ]

    required_fields = {"header": ""}


class VhdlProject(YamlModel):
    """Model of an VHDL project."""

    required_fields = {"project": "", "header": "", "templates": ""}
