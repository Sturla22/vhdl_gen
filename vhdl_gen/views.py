import logging
import pathlib
from abc import ABC
from datetime import datetime
from typing import Any

import jinja2 as jj2
import pkg_resources

from vhdl_gen.filters import datetimeformat
from vhdl_gen.filters import dc_asdict
from vhdl_gen.models import Model
from vhdl_gen.models import YamlModel

logging.basicConfig()
logger = logging.getLogger("views")


def get_subdirs(p: pathlib.Path) -> list[pathlib.Path]:
    return list(filter(pathlib.Path.is_dir, p.iterdir()))


class View(ABC):
    """View of data, responsible for rendering to an output directory.

    Will create the output directory if it doesn't exist.
    """

    output_directory = pathlib.Path("build")

    def render(self, *args, output_directory: pathlib.Path = None, **kwargs) -> None:
        if output_directory is not None:
            self.output_directory = output_directory
        self.output_directory.mkdir(exist_ok=True)


class TemplateView(View):
    """A view that renders a junit2 template to file.

    Line statements and comments are configured to act nice
    with a python syntax highlighters (and black) by default.
    """

    variable_name = "vars"
    line_statement_prefix = "# >"
    line_comment_prefix = "##"
    trim_blocks = True
    lstrip_blocks = False
    block_start_string = "{%"
    block_end_string = "%}"
    variable_start_string = "{{"
    variable_end_string = "}}"
    comment_start_string = "{#"
    comment_end_string = "#}"
    template_filename = ""
    filters = {"datetimeformat": datetimeformat, "dc_asdict": dc_asdict}
    env_globals: dict[str, Any] = {}

    def render(
        self, *args, debug=False, output_directory=None, template=None, **kwargs
    ) -> None:
        super().render(output_directory=output_directory)

        self.env = self.get_environment(*args, **kwargs)
        self.template_filename = self.get_template_filename(template)
        self.add_filters()
        t = self.env.get_or_select_template(str(self.template_filename))

        data = self.add_context(*args, **kwargs)
        result_path = self.output_directory / t.name
        if "result" in data:
            result_path = self.output_directory / data["result"]

        variables = {
            self.variable_name: data,
            "DEBUG": debug,
        }
        self.add_globals(*args, **kwargs)
        stream = t.stream(**variables)
        self.result_path = result_path
        try:
            stream.dump(str(self.result_path))
        except jj2.exceptions.UndefinedError as e:
            logger.exception(f"In {self.template_filename}: {e}")
            raise

    def add_globals(self, *args, **kwargs):
        env_globals = {
            "date": datetime.now(),
            "template_path": self.template_filename,
            "view": self.__class__.__name__,
            "kwargs": kwargs,
            "output_directory": self.output_directory,
        } | self.env_globals
        self.env.globals["env"] = env_globals

    def add_filters(self):
        self.env.filters.update(self.filters)

    def get_template_filename(self, template_filename):
        if template_filename is not None:
            self.template_filename = template_filename
        return self.template_filename

    def get_environment(self, *args, template_directory=[], **kwargs):
        templates = pathlib.Path(
            pkg_resources.resource_filename("vhdl_gen", "data/templates")
        )
        # Loader will look up templates in the given order, user templates override default
        template_directories = template_directory + [templates] + get_subdirs(templates)
        LoggingUndefined = jj2.make_logging_undefined(logger=logger, base=jj2.Undefined)
        paths = [
            str(pathlib.Path(directory).expanduser())
            for directory in template_directories
        ]
        return jj2.Environment(
            loader=jj2.FileSystemLoader(paths),
            undefined=LoggingUndefined,
            # Configurable settings
            trim_blocks=self.trim_blocks,
            lstrip_blocks=self.lstrip_blocks,
            line_statement_prefix=self.line_statement_prefix,
            line_comment_prefix=self.line_comment_prefix,
            block_start_string=self.block_start_string,
            block_end_string=self.block_end_string,
            variable_start_string=self.variable_start_string,
            variable_end_string=self.variable_end_string,
            comment_start_string=self.comment_start_string,
            comment_end_string=self.comment_end_string,
        )

    def add_context(self, context={}, *args, **kwargs):
        return context


class ModelView(TemplateView):
    model: type[Model]

    def add_context(self, *args, **kwargs):
        assert self.model, "ModelView needs a model"
        instance = self.model()
        instance.load(*args, **kwargs)
        return instance.data

    def add_globals(self, *args, **kwargs):
        self.env_globals["model"] = self.model.__name__
        super().add_globals(*args, **kwargs)


class YamlView(ModelView):
    model: Any = YamlModel
