import pathlib
from abc import ABC
from typing import Any

import yaml as pyyaml

from .yaml_constructors import HeaderYamlObject
from .yaml_constructors import IncludeYamlObject
from .yaml_constructors import StrFormatYamlObject
from .yaml_loaders import IncludeLoader


class Model(ABC):
    """Model of data, responsible for loading data."""

    _data: dict[str, Any] = {}

    def load(self, *args, data={}, **kwargs) -> bool:
        """Loads data, returns true if any data was loaded.

        Load is expected to return a dict of dicts which may have
        an arbitrary depth.
        """
        if data:
            self._data = data
        return self.have_data()

    @property
    def data(self) -> dict[str, Any]:
        assert self.have_data(), "data is empty"
        return self._data

    def have_data(self) -> bool:
        return not (self._data == {})


class YamlModel(Model):
    yaml_constructors = [HeaderYamlObject, StrFormatYamlObject, IncludeYamlObject]
    yaml_path = None

    def load(self, *args, yaml: pathlib.Path = None, **kwargs) -> bool:
        """Load data from yaml file if none is provided by data kwarg."""
        if not super().load(*args, **kwargs) and yaml is not None and yaml.is_file():
            self.yaml_path = yaml
            super().load(data=pyyaml.load(yaml.open(), IncludeLoader))
        return self.have_data()
