import argparse
import pathlib

import yaml

from vhdl_gen import vhdl_gen


def path(p):
    return pathlib.Path(p).expanduser()


def configure_parser(description):
    parser = argparse.ArgumentParser(description=description)
    root_dir = pathlib.Path(__file__).parent
    data = yaml.safe_load((root_dir / "arguments.yaml").open())
    for kwargs in data["args"]:
        flags = [f"--{kwargs['name']}"]
        del kwargs["name"]

        if "short" in kwargs:
            flags.append(f"-{kwargs['short']}")
            del kwargs["short"]

        if "type" in kwargs:
            kwargs["type"] = eval(kwargs["type"])

        parser.add_argument(*flags, **kwargs)
    return parser


def main(args=None):
    parser = configure_parser(description=vhdl_gen.main.__doc__)
    options = vars(parser.parse_args(args))
    none_keys = []
    for key, val in options.items():
        if val is None:
            none_keys.append(key)
    for key in none_keys:
        del options[key]
    assert isinstance(options["yaml"], pathlib.Path)
    vhdl_gen.main(options)
