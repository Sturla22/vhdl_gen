import dataclasses
from datetime import datetime


def datetimeformat(value: datetime, fmt="%Y-%m-%d %H:%M"):
    return value.strftime(fmt)


def dc_asdict(dc):
    return dataclasses.asdict(dc)
