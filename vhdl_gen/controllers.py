import dataclasses
import pathlib
from abc import ABC
from typing import Any

import yaml

from vhdl_gen import utils
from vhdl_gen.yaml_loaders import IncludeLoader


class Controller(ABC):
    """Controls views and data preparation."""

    def execute(self, *args, **kwargs):
        view = self.get_view(*args, **kwargs)
        view().render(*args, **kwargs)


@dataclasses.dataclass
class SelectViewController(Controller):
    view_collection: utils.LanguageViewCollection

    def get_view(self, *args, **kwargs):
        return self.view_collection.get_view(*args, **kwargs)


@dataclasses.dataclass
class FileController(SelectViewController):
    def execute(self, *args, **kwargs):
        assert "template" in kwargs, "Need template to render file"
        super().execute(*args, **kwargs)


@dataclasses.dataclass
class ProjectController(SelectViewController):
    def execute(self, *args, **kwargs):
        yaml_data: dict[str, Any] = yaml.load(kwargs["yaml"].open(), IncludeLoader)

        assert "project" in yaml_data, f"key 'project' was not found in {yaml_data}"
        project_data = yaml_data["project"].copy()

        templates = yaml_data["templates"]
        for template_data in templates:
            data = {"project": project_data}
            data |= template_data
            kwargs["template"] = pathlib.Path(template_data["name"])
            FileController(view_collection=self.view_collection).execute(
                *args, data=data, **kwargs
            )
