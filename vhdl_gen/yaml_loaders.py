import pathlib
from typing import IO

import yaml


class IncludeLoader(yaml.FullLoader):
    """YAML Loader with !include constructor."""

    def __init__(self, stream: IO) -> None:
        self._root = pathlib.Path(".")
        if hasattr(stream, "name"):
            self._root = pathlib.Path(stream.name).parent
        super().__init__(stream)


class IgnoreUnknown(yaml.SafeLoader):
    def ignore_unknown(self, node):
        return None

    @classmethod
    def load(cls, path_or_yaml: str) -> None:
        cls.add_constructor(None, cls.ignore_unknown)
        return yaml.load(path_or_yaml, cls)
