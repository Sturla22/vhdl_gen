configuration {{ configuration.name }} of {{ configuration.entity }} is
  --> set for=configuration.for
  for {{ for.name }}
    --> set for=for.for
    for {{ for.name }} : {{ for.use.component }}
      --> set use=for.use
      use entity {{ use.library }}.{{ use.component }}({{ use.architecture }});
    end for;
  end for;
end configuration {{ configuration.name }};

--> if DEBUG:
/*
Generated from configuration.vhd
'configuration' was:
{{ configuration }}
*/
--> endif
