--! \author {{ template.author }}
--! \date {{ env.date | datetimeformat }}
--> for key, value in template.header.extras.items():
--! --% if key %--\{{ key }} --% endif %--{{ value }}
--> endfor
--! \brief {{ template.header.brief }}
--! \detail
--!         {{ template.header.detail | wordwrap(wrapstring='\n--!        ') }}
--> if DEBUG:
--!
--!        Generated from header.vhd
--> endif
