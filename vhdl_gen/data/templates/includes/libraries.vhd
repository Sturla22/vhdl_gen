--> for lib in libraries:
library {{ lib.name }};
--> if lib.use is defined:
--> for use in lib.use:
  use {{ lib.name }}.{{ use }};
--> endfor
--> endif
--> if lib.context is defined:
--> for ctx in lib.context:
  context {{ lib.name }}.{{ ctx }};
--> endfor
--> endif

--> endfor
--> if DEBUG:
/*
Generated from libraries.vhd
'libraries' was:
{{ libraries }}
*/
--> endif
