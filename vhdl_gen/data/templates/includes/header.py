## This is a Junit2 Template for a python header docstring.
"""
@file {{ env.template_path }}
@author {{ vars.author }}
@date {{ env.date }}
# > for key, value in vars.header.extras.items():
{% if key %}@{{ key }} {% endif %}{{ value }}
# > endfor
@brief {{ vars.header.brief }}
@detail
        {{ vars.header.detail | wordwrap(wrapstring='\n        ') }}
# > if DEBUG

       Generated from header.py
# > endif
"""

