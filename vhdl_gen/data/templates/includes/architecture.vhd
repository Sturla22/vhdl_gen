architecture {{ architecture.name }} of {{ entity.name }} is
--% block architecture_definitions %--
--> if architecture.signals is defined:
--% for signal in architecture.signals %--
--> set decl={"id": "signal", "name": signal[0], "type": signal[1], "init": signal[2]}
  --% include "includes/declaration.vhd"%--;
--> if not loop.last:

--> endif
--% endfor %--
--> endif

--> if architecture.constants is defined:
--% for constant in architecture.constants %--
--> set decl={"id": "constant", "name": constant[0], "type": constant[1], "init": constant[2]}
  --% include "includes/declaration.vhd"%--;
--> if not loop.last:

--> endif
--% endfor %--

--> endif

--> if architecture.component_instantiations is defined:
--% for component in architecture.component_instantiations %--
  --> set entity=component.entity
  --% include "includes/entity.vhd" %--
--> if not loop.last:

--> endif
--% endfor %--

--> endif
--% endblock %--
begin
--% block architecture_body %--
--> if architecture.instantiations is defined:
--% for instance in architecture.instantiations %--
--> if instance.library:
  {{instance.label}}: entity {{instance.library.name}}.{{instance.name}}
--> else:
  {{instance.label}}: component {{instance.entity.name}}
--> endif
--> if instance.generic_map
  generic map(
--% for p in instance.generic_map %--
  --> if p | length > 2
    {{ p }} => {{ p }}--% if not loop.last %--,--% endif %--

  --> else
    {{ p[0] }} => {{ p[1] }}--% if not loop.last %--,--% endif %--

  --> endif
--% endfor %--
  )
--> endif
  port map(
--% for p in instance.port_map %--
--> if p | length > 2
    {{ p }} => {{ p }}--% if not loop.last %--,--% endif %--

  --> else
    {{ p[0] }} => {{ p[1] }}--% if not loop.last %--,--% endif %--

  --> endif
--% endfor %--
  );

--% endfor %--
--> endif
--> if architecture.processes is defined:
--% for process in architecture.processes %--
  {{ process.name }}: process--% if process.sensitivity_list %--(--% for sig in process.sensitivity_list %--{{sig}}--% if not loop.last %--,--% endif %----% endfor %--)--% endif %--

begin
--> if process.body is defined:
  {{ process.body }}
--> elif not process.sensitivity_list:
    wait;
--> endif

end process;
--% endfor %--
--> endif
--% endblock %--
end architecture;
--> if DEBUG:
/*
Generated from architecture.vhd
'architecture' was:
{{ architecture | dc_asdict | pprint }}
*/
--> endif

