--% if component is defined %--component--% else %--entity--% endif %-- {{ entity.name }} is
--> if entity.generics:
  --> set generics=entity.generics
  generic(
  --% for generic in generics %--
    --> set decl={"name": generic[0], "type": generic[1], "init": generic[2]}
    --% include "includes/declaration.vhd"%----% if not loop.last %--;--% endif %--

  --% endfor %--
  );
--> endif

--> if entity.ports:
  --> set ports=entity.ports
  port(
  --% for port in ports %--
    --> set decl={"name": port[0], "direction": port[1], "type": port[2], "init": port[3]}
    --% include "includes/declaration.vhd"%----% if not loop.last %--;--% endif %--

  --% endfor %--
  );
--> endif
--% if not component is defined %--begin--% endif %--

--> if "entity_body" in entity:
  {{ entity.entity_body }}
--> endif
end --% if component is defined %--component--% else %--entity--% endif %--;
--> if DEBUG:
/*
Generated from entity.vhd
'entity' was:
{{ entity }}
*/
--> endif
