{# This is a Junit2 Template for a VUnit run file. #}
{# Quick check that we have the right model, this will be ignored if all is right: #}
{# {{ template_error_wrong_model_used.anything }} #}
#!/usr/bin/env python3
# > include "includes/header.py"

from vunit import VUnit


def configure_vunit(vu):
    lib = vu.add_library("{{ vars.project.name }}")
    lib.add_source_files("{{ env.output_directory }}/*.vhd")
    return lib


if __name__ == "__main__":
    vu = VUnit.from_argv()
    configure_vunit(vu)
    # Run vunit function
    vu.main()
