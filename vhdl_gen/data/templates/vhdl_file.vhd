--# This is a Junit2 Template for vhdl files. #--
--# Quick check that we have the right model, this will be ignored if all is right: #--
--# {{ template_error_wrong_model_used.anything }} #--
--> set template=vars
--% block header %--
--> include "includes/header.vhd"
--% endblock %--
--% block file_body %--
--% endblock %--
--> if DEBUG:
/*
This file was generated from vhdl_file.vhd at {{ env.date }}
The data came from {{ env.kwargs.yaml }}
The model used was {{ env.model }}
The view used was {{ env.view }}
The kwargs were:
{{ env.kwargs | pprint }}
*/
--> endif
