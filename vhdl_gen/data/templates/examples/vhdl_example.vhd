--> set template=vars
--> include "includes/header.vhd"

--> set libraries=template.libraries
--> include "includes/libraries.vhd"

--> set entity=template.entity
--> include "includes/entity.vhd"

--> set architecture=template.architecture
--> include "includes/architecture_"+ architecture.name +".vhd"

-- This file was generated from vhdl_example.vhd at {{ env.date }}
