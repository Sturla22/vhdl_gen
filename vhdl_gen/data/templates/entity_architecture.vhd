--% extends "vhdl_file.vhd" %--
--% block file_body %--
--> set libraries=template.libraries
--> if libraries:
--> include "includes/libraries.vhd"
--> endif

--> set entity=template.entity
--> if entity:
--> include "includes/entity.vhd"
--> endif

--> set architecture=template.architecture
--> if architecture:
--> include "includes/architecture_"+ architecture.name +".vhd"
--> endif

--> if DEBUG:
-- Generated from entity_architecture.vhd
--> endif
--% endblock %--
