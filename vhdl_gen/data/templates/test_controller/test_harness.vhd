--# Quick check that we have the right model, this will be ignored if all is right: #--
--# {{ template_error_wrong_model_used.anything }} #--
--% extends "vhdl_file.vhd" %--
--% block file_body %--
--> set libraries=template.libraries
--> if libraries:
--> include "includes/libraries.vhd"
--> endif

--> set entity=template.entity
--> if entity:
--> include "includes/entity.vhd"
--> endif

--> set architecture=template.architecture
--> if architecture:
--> include "includes/architecture_structural.vhd"
--> endif

--> if DEBUG:
-- Generated from test_harness.vhd
--> endif
--% endblock %--
