--% extends "vhdl_file.vhd" %--
--% block file_body %--
--> if template.libraries is defined:
--> set libraries=template.libraries
--> include "includes/libraries.vhd"
--> endif

--> if template.entity is defined:
--> set entity=template.entity
--> endif
--> if template.architecture is defined:
--> set architecture=template.architecture
--> include "includes/architecture.vhd"
--> endif

--> if template.configuration is defined:
--> set configuration=template.configuration
--> include "includes/configuration.vhd"
--> endif

--> if DEBUG:
-- Generated from test_case.vhd
--> endif
--% endblock %--
