from typing import Any

from vhdl_gen import utils
from vhdl_gen import yaml_constructors  # noqa: F401
from vhdl_gen.controllers import FileController
from vhdl_gen.controllers import ProjectController
from vhdl_gen.controllers import SelectViewController
from vhdl_gen.vhdl_views import VhdlFileView
from vhdl_gen.views import YamlView
from vhdl_gen.yaml_loaders import IgnoreUnknown

language_view_collection = utils.LanguageViewCollection(
    default_view=YamlView,
    view_choices=[
        utils.LanguageViewChoice(pattern=r"^.+\.vhd?$", view=VhdlFileView, name="vhdl")
    ],
)


def is_project(yaml) -> bool:
    if yaml.is_file():
        data = IgnoreUnknown.load(yaml.open())
        if data:
            return "project" in data and "templates" in data
    return False


def main(options: dict[str, Any]) -> None:
    """VHDL Generator, generate code based on templates and yaml data."""
    controller: type[SelectViewController] = FileController
    if is_project(options["yaml"]):
        controller = ProjectController
    controller(view_collection=language_view_collection).execute(**options)
