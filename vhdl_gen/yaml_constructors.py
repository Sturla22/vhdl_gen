import dataclasses
import typing

import yaml

from vhdl_gen import yaml_loaders


class YamlObject(yaml.YAMLObject):
    """A wrapper for YAMLObject that calls the __post_init__ of
    dataclasses on yaml load and validates fields.

    Should be subclassed with dataclasses.
    Checks self.required_fields and field types on the top level.
    """

    required_fields: list[str] = []

    @classmethod
    def from_yaml(cls, loader, node):
        fields = loader.construct_mapping(node, deep=True)
        return cls(**fields)

    def is_required(self, field):
        required = field.name in self.required_fields
        if required:
            assert hasattr(self, field.name), f"Field {field.name} is required."
        return required

    def check_match(self, field, expected, actual):
        if not self.is_required(field) and not actual:
            return
        active_class = self.__class__.__name__.replace("YamlObject", "")
        assert (
            expected == actual
        ), f"({active_class}) {expected=} != {actual=} for {field}"

    def check_field(self, field):
        actual_type = type(getattr(self, field.name))
        expected_type = field.type

        expected_origin = typing.get_origin(expected_type)
        actual_origin = typing.get_origin(actual_type)
        self.check_match(field, expected_origin, actual_origin)

        expected_args = typing.get_args(expected_type)
        actual_args = typing.get_args(actual_type)
        self.check_match(field, expected_args, actual_args)

    def __post_init__(self):
        fields = dataclasses.fields(self)
        for field in fields:
            self.check_field(field)

    def __iter__(self):
        assert dataclasses.is_dataclass(self), "Cannot iterate over this YamlObject"
        self.fields = iter(dataclasses.fields(self))
        return self

    def __next__(self):
        return next(self.fields)


@dataclasses.dataclass
class IncludeYamlObject(YamlObject):
    """Include other yaml files.

    Example:
    ---
    !include: some.yaml
    """

    yaml_tag = "!include"

    @classmethod
    def from_yaml(cls, loader, node):
        """Load included yaml."""
        assert isinstance(
            loader, yaml_loaders.IncludeLoader
        ), "Need to load with the IncludeLoader"
        path = loader._root / loader.construct_scalar(node)
        if not path.exists() or not path.is_file():
            raise FileNotFoundError(f"Could not find file '{path}'")
        if path.suffix not in (".yaml", ".yml"):
            raise ValueError(f"Unsupported file type '{path.suffix}'.")
        with path.open() as f:
            return yaml.load(f, yaml_loaders.IncludeLoader)


@dataclasses.dataclass
class StrFormatYamlObject(YamlObject):
    """String formatting in yaml.

    Example:
    ---
    !str_format:
        s: "{}, {}!"
        args:
          - hello
          - world
    """

    yaml_tag = "!str_format"

    s: str
    args: list

    @classmethod
    def from_yaml(cls, loader, node):
        d = super().from_yaml(loader, node)
        return d.s.format(*d.args)


@dataclasses.dataclass
class HeaderYamlObject(YamlObject):
    """File header representation.

    Example:
    ---
    !header:
      brief: short description
      detail: long description
      extras:
        ref: myref
    """

    yaml_tag = "!header"

    brief: str = ""
    detail: str = ""
    extras: dict[str, str] = dataclasses.field(default_factory=dict)


@dataclasses.dataclass
class EntityYamlObject(YamlObject):
    """VHDL Entity representation.

    Example:
    ---
    !entity:
      name: example_e
      ports:
        - [clk, in, std_ulogic]
      generics:
        - [G_DEBUG, integer, "0"]
    """

    yaml_tag = "!entity"
    required_fields = ["name"]

    name: str
    ports: list[str] = dataclasses.field(default_factory=list)
    generics: list[str] = dataclasses.field(default_factory=list)


@dataclasses.dataclass
class ArchitectureYamlObject(YamlObject):
    """VHDL Architecture representation.

    Example:
    ---
    !architecture:
      name: structural
      constants:
        - [C_WORLD, string, '"world"']
    """

    yaml_tag = "!architecture"
    required_fields = ["name"]

    name: str
    constants: list[str] = dataclasses.field(default_factory=list)
    signals: list[str] = dataclasses.field(default_factory=list)
    processes: list[str] = dataclasses.field(default_factory=list)
    instantiations: list[str] = dataclasses.field(default_factory=list)
    component_instantiations: list[str] = dataclasses.field(default_factory=list)


@dataclasses.dataclass
class VhdlFileYamlObject(YamlObject):
    """VHDL File representation.

    Example:
    ---
    !VhdlFile
    author: Sturla Lange
    !header:
      extras:
        ref: myref
      detail: long description
      brief: short description
    libraries:
      ieee:
        std_logic_1164:
          - all
    !entity:
      name: example_e
      ports:
        - [clk, in, std_ulogic]
      generics:
        - [G_DEBUG, integer, "0"]
    !architecture:
      name: structural
      constants:
        - [C_WORLD, string, '"world"']
    """

    yaml_tag = "!VhdlFile"

    author: str = ""
    header: HeaderYamlObject = dataclasses.field(default_factory=HeaderYamlObject)
    libraries: dict[str, dict[str, list]] = dataclasses.field(default_factory=dict)
    entity: EntityYamlObject = dataclasses.field(default_factory=EntityYamlObject)  # type: ignore
    architecture: ArchitectureYamlObject = dataclasses.field(
        default_factory=ArchitectureYamlObject  # type: ignore
    )
