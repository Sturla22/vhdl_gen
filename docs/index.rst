.. You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VHDL Generator's documentation!
================================================

.. toctree::
    contributing.rst

.. mdinclude:: ../README.md
