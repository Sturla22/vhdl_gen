import pathlib

from vhdl_gen.vhdl_views import VhdlFileView


def test_yaml_to_vhdl_basic():
    instance = VhdlFileView()
    instance.render(
        yaml=pathlib.Path("examples/vhdl_file_example.yaml"),
        debug=True,
    )
