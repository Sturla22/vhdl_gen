import pathlib

import pytest
import yaml

from vhdl_gen.yaml_loaders import IncludeLoader


def test_include_loader():
    p_org = pathlib.Path("examples/example.yml")
    d_vanilla = yaml.safe_load(p_org.open())
    d_org = yaml.load(p_org.open(), IncludeLoader)
    assert d_org == d_vanilla
    assert "author" in d_org
    d = yaml.load(f"!include {p_org}", IncludeLoader)
    assert d == d_org


def test_include_constructor_yaml():
    yaml.load("!include examples/example.yaml", IncludeLoader)


def test_include_construct_no_file():
    with pytest.raises(FileNotFoundError):
        yaml.load("!include value", IncludeLoader)


def test_construct_directory():
    with pytest.raises(FileNotFoundError):
        yaml.load("!include examples/", IncludeLoader)


def test_construct_not_yaml():
    with pytest.raises(ValueError):
        yaml.load(
            "!include vhdl_gen/data/templates/examples/example.txt", IncludeLoader
        )
