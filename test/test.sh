#!/usr/bin/env bash

set -ex

# A command wrapper to handle local tests (when the script is not installed).
_vhdl_gen() {
    if hash vhdl_gen 2>/dev/null; then
        vhdl_gen $@
    else
        python -m vhdl_gen $@
    fi
}

_vhdl_gen -h
_vhdl_gen -d --yaml examples/example.yaml --template example.txt
_vhdl_gen -d --yaml examples/example.yaml --template example.txt --template-directory examples/other_template_dir  -o build/tmp
_vhdl_gen -d --yaml examples/vhdl_file_example.yaml --template vhdl_example.vhd
_vhdl_gen -d --language vhdl --yaml examples/vhdl_file_example.yaml --template vhdl_example.vhd
_vhdl_gen -d --language vhdl --yaml examples/vhdl_file_example.yaml --template entity_architecture.vhd
_vhdl_gen -d --yaml examples/vhdl_project_example.yaml

_vhdl_gen -d --yaml examples/test_controller.yaml -o build/test_controller
chmod +x ./build/test_controller/run.py  && ./build/test_controller/run.py -o build/vunit_out
# vsg -f ./build/test_controller/*.vhd
