from unittest.mock import Mock

import pytest

from vhdl_gen import views as dut


class AView(dut.View):
    pass


def test_view():
    a = AView()
    a.render()


def test_view_outputdirectory(monkeypatch):
    a = AView()
    path_mock = Mock()
    a.render(output_directory=path_mock)


def test_template_view_undefined(monkeypatch):
    t = dut.TemplateView()
    with pytest.raises(Exception):
        t.render(template="example.txt", data={"hi", "bye"})
