import pathlib

import pytest

from vhdl_gen import vhdl_gen as dut

default_options = {
    "yaml": pathlib.Path(""),
}


def test_vhdl_gen_main_empty():
    with pytest.raises(AssertionError):
        dut.main(default_options)


def test_vhdl_gen_main_no_yaml():
    options = default_options.copy()
    options["template"] = pathlib.Path("example.txt")
    with pytest.raises(AssertionError):
        dut.main(options)


def test_vhdl_gen_main_basic():
    options = default_options.copy()
    options["template"] = pathlib.Path("example.txt")
    options["yaml"] = pathlib.Path("examples/example.yaml")
    dut.main(options)


def test_vhdl_gen_main_vhdl():
    options = default_options.copy()
    options["template"] = pathlib.Path("vhdl_example.vhd")
    options["yaml"] = pathlib.Path("examples/vhdl_file_example.yaml")
    options["language"] = "vhdl"
    dut.main(options)


def test_vhdl_gen_main_project():
    options = default_options.copy()
    options["yaml"] = pathlib.Path("examples/vhdl_project_example.yaml")
    dut.main(options)


def test_vhdl_gen_main_test_ctrlr():
    options = default_options.copy()
    options["yaml"] = pathlib.Path("examples/test_controller.yaml")
    options["template_directory"] = [
        pathlib.Path("templates"),
        pathlib.Path("templates/test_controller"),
    ]
    options["output_directory"] = pathlib.Path("build/test_controller")
    dut.main(options)


def test_vhdl_gen_main_haskell():
    options = default_options.copy()
    options["template"] = pathlib.Path("example.txt")
    options["yaml"] = pathlib.Path("examples/example.yaml")
    options["language"] = "haskell"
    with pytest.raises(UserWarning):
        dut.main(options)
