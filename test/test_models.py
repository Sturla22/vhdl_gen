import hypothesis as ht
import pytest

from vhdl_gen import models as dut


class AModel(dut.Model):
    pass


def test_model_default_no_load():
    a = AModel()
    a.load("")
    with pytest.raises(AssertionError):
        assert a.data == {}


@ht.given(
    ht.strategies.dictionaries(keys=ht.strategies.text(), values=ht.strategies.text())
)
def test_model_default_load(d):
    ht.assume(d)
    b = AModel()
    b.load(data=d)
    assert b.data == d
