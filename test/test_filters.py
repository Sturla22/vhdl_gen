import datetime

from dateutil.parser import isoparse
from hypothesis import given
from hypothesis import strategies

from vhdl_gen import filters as dut


def test_datetimeformat():
    d = datetime.datetime.fromisoformat("2021-04-17 19:00:01.123456")
    assert dut.datetimeformat(d) == "2021-04-17 19:00"
    assert dut.datetimeformat(d, "%Y") == "2021"


@given(strategies.datetimes())
def test_datetime_hypothesis(s):
    date_str = dut.datetimeformat(s)

    # Ignore any error caughed up by dateutil
    try:
        parsed = isoparse(date_str)
    except Exception:
        return

    assert abs(parsed - s) < datetime.timedelta(minutes=1)
