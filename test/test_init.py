import pytest

import vhdl_gen as dut


arg_lists = [
    s.split(" ")
    for s in [
        "-d --yaml examples/example.yaml --template example.txt",
        "-d --yaml examples/example.yaml --template example.txt --template-directory examples/other_template_dir -o build/tmp",
        "-d --yaml examples/vhdl_file_example.yaml --template vhdl_example.vhd",
        "-d --language vhdl --yaml examples/vhdl_file_example.yaml --template vhdl_example.vhd",
        "-d --language vhdl --yaml examples/vhdl_file_example.yaml --template entity_architecture.vhd",
        "-d --yaml examples/vhdl_project_example.yaml",
        "-d --yaml examples/test_controller.yaml -o build/test_controller",
    ]
]


@pytest.mark.parametrize("args", arg_lists)
def test_main(args):
    dut.main(args)
